#include <stdio.h>

int main(){
    float lado_a,lado_b,lado_c,perimetro;
    
    printf("Ingresa el valor del primer lado del triangulo\n");
	scanf("%f", &lado_a);
	while(lado_a<1){
		printf("Tama�o invalido\n");
		scanf("%f", &lado_a);
	}
	
	printf("Ingresa el valor del segundo lado del triangulo\n");
	scanf("%f", &lado_b);
	while(lado_b<1 || lado_b == lado_a){
		printf("Tama�o invalido, n�mero 0 o negativo o lado igual a uno anterior\n");
		scanf("%f", &lado_b);
	}
	
	printf("Ingresa el valor del tercer lado del triangulo\n");
	scanf("%f", &lado_c);
	while(lado_c<1 || lado_c == lado_a || lado_c == lado_b){
		printf("Tama�o invalido, n�mero 0 o negativo o lado igual a uno anterior\n");
		scanf("%f", &lado_c);
	}
	
	perimetro = lado_a + lado_b + lado_c;
    printf("Perimetro: %.2f\n",perimetro);
}
