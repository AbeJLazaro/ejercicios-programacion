#include <stdio.h>

int main(){
    float lado_corto,lado_largo,perimetro;
    
    printf("Ingresa el valor del lado diferente del triangulo\n");
	scanf("%f", &lado_corto);
	while(lado_corto<1){
		printf("Tama�o invalido\n");
		scanf("%f", &lado_corto);
	}
	
	printf("Ingresa el tama�o del los dos lados iguales del triangulo\n");
	scanf("%f", &lado_largo);
	while(lado_largo<1 || lado_largo == lado_corto){
		printf("Tama�o invalido, n�mero 0 o negativo o todos los lados son iguales\n");
		scanf("%f", &lado_largo);
	}
	
	perimetro = (lado_largo * 2) + lado_corto;
    printf("Perimetro: %.2f\n",perimetro);
}
