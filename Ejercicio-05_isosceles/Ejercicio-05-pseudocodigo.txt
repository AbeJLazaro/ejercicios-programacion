inicio
    variable lado_corto es flotante
    variable lado_largo es flotante
    variable perimetro es flotante 

    Imprimir "Ingresa el valor del lado diferente del triangulo"
    leer lado_corto 

    mientras (lado_corto < 1)
        Imprimir "Tamaño invalido"
        leer lado_corto
    fin-mientras 

    Imprimir "Ingresa el tamaño de los dos lados iguales del triangulo"
    leer lado_largo 

    mientras (lado_largo < 1 or lado_largo==lado_corto)
        Imprimir "Tamaño invalido, número 0 o negativo o todos los lados son iguales"
        leer lado_largo
    fin-mientras 

    perimetro = (lado_largo * 2) + lado_corto
    imprimir "Perimetro: ", perimetro 
fin