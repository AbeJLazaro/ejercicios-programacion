#include <stdio.h>

int main(){
    float lado,perimetro;
    
    printf("Ingresa el valor del lado del triangulo equilatero\n");
	scanf("%f", &lado);
	while(lado<1){
		printf("Tama�o invalido\n");
		scanf("%f", &lado);
	}
	
	perimetro = lado * 3;
    printf("Perimetro: %.2f\n",perimetro);
}
