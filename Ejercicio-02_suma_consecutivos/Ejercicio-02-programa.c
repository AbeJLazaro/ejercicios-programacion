#include <stdio.h>

int main(){
    int tope, conteo, total;
    printf("Ingresa un numero del 1 al 50\n");
	scanf("%d", &tope);
    while(tope < 1 || tope > 50){
        printf("N�mero no dentro del rango 1-50\n");
        scanf("%d", &tope);
    }
    
    total = 0;
    conteo = 1;
    while(conteo<=tope){
    	total += conteo;
    	conteo += 1;
    }
    printf("El valor total es: %d\n",total);
}
